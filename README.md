# 本项目的名称：harmonyOSWatchDemo

#### 介绍

> 本项目是照着 张荣超 老师视频教程的实操
>
> 这是开发鸿蒙系统 APP 的教程，本教程是开发一个在鸿蒙运动手表运行的呼吸训练的APP

本示例代码，不准备让大家克隆了运行，本教程希望大家照下面任一教程，进行实操
1. 视频链接：[https://www.yuque.com/chatterzhao/harmonyos/vtgzm6](https://www.yuque.com/chatterzhao/harmonyos/vtgzm6)
2. 文档教程：[https://www.yuque.com/chatterzhao/harmonyos/fq13ri](https://www.yuque.com/chatterzhao/harmonyos/fq13ri)

如果撸代码出现怎么撸都不一致，建议你启用 Git 版本控制，先把你的代码 git commit ，然后复制我们教程的代码，然后变化了的文件可以在这里看到，右键具体文件，点击 Show Diff，就可以看到改了哪里（哪里不同）
![输入图片说明](https://images.gitee.com/uploads/images/2020/0917/104924_d549d7f3_2041416.png "屏幕截图.png")